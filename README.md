# Docker academy course repo 

Description of the content.

## docker-academy-env
All sources used to create the course vagrant box. We used ansible to 
provision the box.

## dockerfiles
Dockerfiles used in our labs

## labs
The labs with instructions and solutions.

## lab-server
The ansible scripts used to set up the lab server with the docker registry 
and docker registry mirror.

## Tips
### connect via ssh
If you want to connect via ssh, but not with vagrant for reasons like easy copy paste etc,

`ssh://academy:<askSpeakerForPW>@10.0.0.100`
`sudo -s`