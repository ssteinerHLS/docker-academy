# Solutions

### Task 1

```
$ docker run -d -p 9000:9099 --name docker-presentation registry:5000/nodeserver:1.0
$ docker inspect --format="{{.Config.WorkingDir}}" docker-presentation
```


### Task 2

```
$ cd /root
$ git clone https://bitbucket.org/ti8m/docker-academy-app.git
$ cd docker-academy-app
$ docker run --rm -it -v $PWD:/app:z  maven:3.3-jdk-8 mvn -f /app/pom.xml clean install
```
