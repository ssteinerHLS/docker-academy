# Jenkins

## Build and deploy with Jenkins


* download jenkins home to bootstrap configuration

```
cd /var
curl -L  -o jenkins_home.tar.gz https://www.dropbox.com/s/lplrrfh8chunemk/jenkins_home.tar.gz?dl=0
tar -zxvf jenkins_home.tar.gz
mv _data jenkins_home
```

* start academy-jenkins
```
docker run -v /var/jenkins_home:/var/jenkins_home:z -p 8085:8080 -d registry:5000/academy-jenkins:1.0
```

Jenkins should now be available under [http://10.0.0.100:8085/](http://10.0.0.100:8085/)