# Lab

In this lab we configure your academy lab vm accordingly and clone the 
required git repositories. We recommend to clone it on your laptop and 
not from inside the lab vm.

## Course resources

###  docker-academy-public
You should already have this repo clone in preparation for this course.
It contains the Vagrantfile and setup guide. 

The content of the docker-academy-public folder is also available within
the Vagrant box. Therefore we checkout all subsequent repositories in 
this folder. 

git clone https://bitbucket.org/ti8m/docker-academy-public.git

###  docker-academy

This repo contains our labs, dockerfiles and also the ansible provisioning 
scripts used to prepare the academy Vagrant box.

```sh
$ cd .../docker-academy-public 
$ git clone https://bitbucket.org/ti8m/docker-academy.git
$ echo docker-academy/ >> .gitignore
```

###  docker-academy-app

This repo contains our academy demo application which will be dockerized 
throughout the course.

```sh
$ cd .../docker-academy-public 
$ git clone https://bitbucket.org/ti8m/docker-academy-app.git
$ echo docker-academy-app/ >> .gitignore
```

### docker-academy-workshop-slides

Here we have the slides of the course.

```sh
$ cd .../docker-academy-public 
$ git clone https://bitbucket.org/ti8m/docker-academy-workshop-slides.git
$ echo docker-academy-workshop-slides/ >> .gitignore
```

## Docker engine configuration

First we need to make sure your docker engine is able to connect to our
lab registry server and registry mirror. As we do not have an official 
SSL certificate for our lab registry we need to add it as `insecure-registry`. 

> A registry mirror is basically the same as a http proxy. It caches the
 docker images so that it is only downloaded once form the internet.
 
SSH into the vagrant box and switch to root.

```sh
vagrant ssh
sudo su -
```

As we do not have a DNS entry for our registry we need to put in into 
the lab vm /etc/hosts manually.

```shell
echo 10.10.19.19   registry >> /etc/hosts
```

Now we open the config file and adjust the settings.
 
```sh
vi /etc/sysconfig/docker-engine

# change mirror to:
REGISTRY_MIRROR='--registry-mirror=http://registry:5001'

# change insecure registry to:
INSECURE_REGISTRY='--insecure-registry=registry:5000 --insecure-registry=registry:5001'

# restart the docker engine
systemctl restart docker

# test the change setup
docker pull busybox:1.24.1
```

